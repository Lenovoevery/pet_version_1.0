package com.yianke.pet.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;

import com.yianke.pet.R;
import com.yianke.pet.base.BaseActivity;
import com.yianke.pet.utils.BaseHandler;
import com.yianke.pet.utils.StatusBarUtils;

/**
 * 界面启动页
 */
public class SplashActivity extends BaseActivity
{

    private BaseHandler mHandler = new BaseHandler();
    private Runnable mRun = new Runnable()
    {
        @Override
        public void run()
        {
            Intent it = new Intent(SplashActivity.this, LeaderActivity.class);
            startActivity(it);
            finish();
        }
    };


    @Override
    protected int getResourceId()
    {
        return R.layout.activity_splash;
    }

    @Override
    public void initView()
    {
        super.initView();
        StatusBarUtils.setColorNoTranslucent(this, Color.WHITE);
        //关闭导航栏
        final View decorView = getWindow().getDecorView();
        final int uiOption = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOption);
        mHandler.postDelayed(mRun, 1500);
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (mHandler != null && mRun != null)
        {
            mHandler.removeCallbacks(mRun);
        }
        getWindow().setBackgroundDrawable(null);
    }
}
